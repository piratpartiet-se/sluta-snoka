var c_primary = '#512483';
var c_secundary = '#f28903';
var c_bg1 = '#b79200';
var c_bg2 = '#6a8216';
var c_bg3 = '#a52268';

function showPage() {
    var welcome_screen = document.getElementById("welcome");
    welcome_screen.style.display = 'none';

    var page = document.getElementById("main");
    page.classList.remove("d-none");

    console.log("Show page...");

    page.style.opacity = 1;
}

var isMobile = window.matchMedia("only screen and (max-width: 992px)");

if (isMobile.matches) {
    showPage();
}